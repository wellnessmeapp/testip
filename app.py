from flask import Flask, jsonify
from flask_restful import Api
from dotenv import load_dotenv
from auth.auth import auth_bp
app = Flask(__name__)
load_dotenv(".env", verbose=True)
app.config.from_envvar(
    "APPLICATION_SETTINGS"
)  # override with config.py (APPLICATION_SETTINGS points to config.py)

api = Api(app)

app.register_blueprint(auth_bp)

if __name__ == "__main__":
    app.run(port=5000, debug=True, host='0.0.0.0')