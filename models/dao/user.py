from models.user import UserModel


class UserDAO:
    model = None

    def __init__(self, model):
        self.model = model

    @property
    def get_last_ip_connection(self) -> "String":
        pass


user_dao = UserDAO(UserModel())
