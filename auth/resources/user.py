import os
from logfile import logger
from flask import request, make_response
from flask_restful import Resource
from libs.strings import gettext
from services.user import user_register_service

DEV_NAME_INITIAL = "JP"


class UserRegister(Resource):
    """
    This is for get the request from client
    """
    @classmethod
    def get(cls, name: str = None):
        """
        Get the name parameter and build the response
        :param name: String
        :return: resp : Object
        """
        logger.info("starting process")
        try:
            ip_address = request.remote_addr
            user_response = user_register_service.get_connection_response(name, ip_address)
            resp = make_response(user_response)
            resp.headers["x-hello-world"] = DEV_NAME_INITIAL
            return resp
        except Exception as e:
            return {"message": gettext("process_failed")}, 401

