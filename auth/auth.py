from flask import Blueprint
from flask_restful import Api
from auth.resources.user import UserRegister

auth_bp = Blueprint('auth_bp', __name__)
api = Api(auth_bp)

api.add_resource(UserRegister, "/ip_address/<string:name>", "/ip_address")
