from libs.strings import gettext
from logfile import logger


class UserService:
    def get_connection_response(self, name: str, ip: str):
        """
        Creates the structured need with the ip and the user name
        :param name:
        :param ip:
        :return:
        """
        logger.info(f"new connection from {ip}")
        response = {'ip': ip}
        if name is not None:
            response["greeting"] = gettext("user_greeting").format(name)
        return response


user_register_service = UserService()