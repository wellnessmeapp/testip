import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)s %(levelname)s:%(message)s',
                    filename='logs/log.log')
logger = logging.getLogger(__name__)